<!-- Footer
============================================= -->
<footer id="footer" class="dark" style="background-color: rgb(38,50,94);;">

  <div class="container">

    <!-- Footer Widgets
    ============================================= -->
    <div class="footer-widgets-wrap clearfix">

      <div class="col_one_fourth">
        <center>
          <h2 class="asl-font dark">SIGUENOS EN</h2>
        </center>
      </div>

      <div class="col_one_fourth">
        <center>
          <h3 class="dark"><i class="icon-facebook"></i><br />AtletideSanLuis</h3>
        </center>
      </div>

      <div class="col_one_fourth">
        <center>
          <h3 class="dark"><i class="icon-twitter"></i><br />@AtletideSanLuis</h3>
        </center>
      </div>

      <div class="col_one_fourth col_last">
        <center>
          <h3 class="dark"><i class="icon-instagram2"></i><br />AtletideSanLuis</h3>
        </center>
      </div>

    </div><!-- .footer-widgets-wrap end -->

  </div>

  <!-- Copyrights
  ============================================= -->
  <div id="copyrights">

    <div class="container clearfix">

      <div class="col_full col_last" style="margin-bottom: 0;">
        <div class="copyrights-menu copyright-links clearfix">
          <a href="#">Términos y Condiciones</a>/<a href="#">Aviso de Privacidad</a>
        </div>
      </div>

    </div>

  </div><!-- #copyrights end -->

</footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/plugins.js"></script>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/functions.js"></script>

<!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>

<script type="text/javascript">

  var tpj=jQuery;
  tpj.noConflict();

  tpj(document).ready(function() {

    var apiRevoSlider = tpj('.tp-banner').show().revolution(
    {
      sliderType:"standard",
      sliderLayout:"fullwidth",
      delay:9000,
      navigation: {
        arrows:{enable:true}
      },
      responsiveLevels:[1240,1024,778,480],
      visibilityLevels:[1240,1024,778,480],
      gridwidth:[1140,1140,778,480],
      gridheight:[600,600,960,600],
      touchenabled:"on",
      onHoverStop:"on",
    });

    apiRevoSlider.bind("revolution.slide.onloaded",function (e) {
      setTimeout( function(){ SEMICOLON.slider.sliderParallaxDimensions(); }, 400 );
    });

    apiRevoSlider.bind("revolution.slide.onchange",function (e,data) {
      SEMICOLON.slider.revolutionSliderMenu();
    });

  });

  tpj(document).ready(function() {

    var owl = tpj("#asl-calendar-carousel");

    owl.owlCarousel({
      items : 1,

      nav : true,
      navText: ['<i class="icon-angle-left" />','<i class="icon-angle-right" />'],
      rewindNav : true,
      scrollPerPage : true,

      pagi : false,

      autoPlay : false,
      mouseDrag : false,
      touchDrag : false,
    });

    tpj('iframe[src*="embed.spotify.com"]').each( function() {
      tpj(this).css('width',tpj(this).parent(1).css('width'));
      tpj(this).attr('src',tpj(this).attr('src'));
    });

    tpj(window).resize(function() {
      tpj('iframe[src*="embed.spotify.com"]').each( function() {
        tpj(this).css('width',tpj(this).parent(1).css('width'));
        tpj(this).attr('src',tpj(this).attr('src'));
      });

    });

    tpj('#tab-responsive-1 .table-responsive table').addClass('table table-stripped');

  });

</script>

</body>
</html>

<!--
<div class="clear"></div>
</div>
<footer id="footer" role="contentinfo">
<div id="copyright">
<?php echo sprintf( __( '%1$s %2$s %3$s. All Rights Reserved.', 'blankslate' ), '&copy;', date( 'Y' ), esc_html( get_bloginfo( 'name' ) ) ); echo sprintf( __( ' Theme By: %1$s.', 'blankslate' ), '<a href="http://tidythemes.com/">TidyThemes</a>' ); ?>
</div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
