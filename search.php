<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : ?>
  <section id="page-title">

    <div class="container clearfix">
      <h1><center><strong><?php printf( __( 'Resultados para: %s', 'blankslate' ), get_search_query() ); ?></strong></center></h1>
    </div>

  </section><!-- #page-title end -->
  <style>
  a.search-title:hover {
    color: #7eb31f !important;
  }
  </style>
<?php while ( have_posts() ) : the_post(); ?>
  <!-- Content
  ============================================= -->
  <section id="content">

    <div class="content-wrap">

      <div class="container clearfix">

        <div id="posts" class="small-thumbs">

          <div class="entry clearfix">
            <div class="entry-c">
              <div class="entry-title">
                <h2><a href="<?php echo get_the_permalink(); ?>" class="search-title"><?php the_title(); ?></a></h2>
              </div>
              <div class="entry-content">
                <p><?php the_excerpt(); ?></p>
                <a href="<?php echo get_the_permalink(); ?>" class="button button-default">Leer más...</a>
              </div>
            </div>
          </div>

        </div>

      </div>

    </div>

  </section>
<?php endwhile; ?>
<?php get_template_part( 'nav', 'below' ); ?>
<?php else : ?>
<article id="post-0" class="post no-results not-found">
<header class="header">
<h2 class="entry-title"><?php _e( 'No se obtuvo resultados', 'blankslate' ); ?></h2>
</header>
<section class="entry-content">
<p><?php _e( 'Lo sentimos, nada coincidió con tu búsqueda. Por favor inténtalo de nuevo.', 'blankslate' ); ?></p>

</section>
</article>
<?php endif; ?>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
