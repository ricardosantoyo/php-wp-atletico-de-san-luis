<style>
@media (max-width: 991px){
	#newsletter {
		background-position: 50%;
	}
}
</style>
<!--inicio de suscríbete -->

			<div id="newsletter" class="section nobottommargin" style="background-image:url(<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/fondosuscribete.jpg); background-size: cover; width:100%; height: 400px; margin-top:0px;">
					<div class="container clearfix" style="margin-top: 60px;">

						<div class="heading-block center">
							<h3 style="color:#5d5d5d;">Suscríbete  <span style="color:#702982;">a nuestro</span> newsletter</h3>
						</div>

						<div class="subscribe-widget">
							<div class="widget-subscribe-form-result"></div>
							<form id="widget-subscribe-form2" action="include/subscribe.php" role="form" method="post" class="nobottommargin">
								<div class="input-group input-group-lg divcenter" style="max-width:600px;">
									<span class="input-group-btn">
										<center>
											<a class="button button-default button-reveal button-xlarge" href="https://visitor.r20.constantcontact.com/manage/optin?v=001uL6aNbggBXJV78LHQNeUTBXTUOdZiTGpXIWyA3L5RceT5rbIm0bvNWXHLEOUKi20HBiftVFdO7JCi6XmvSntcX6LSr0UNoEcWT4PDpqJS87Y0zz8fAAXI5JBCx4MIC9kys2ThDLDKszznHqDvWCO_h-Kv24Zlc-N" target="_blank"><i class="icon-email2"></i> <span>Suscríbete ahora</span></a>
										</center>
									</span>
								</div>
							</form>
						</div>

					</div>
				</div>

			<!--fin de suscríbete -->
