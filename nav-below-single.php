<div class="line"></div>

<!-- Post Navigation
============================================= -->
<div class="post-navigation clearfix">

  <div class="col_half nobottommargin">
    <?php previous_post_link( '%link', '&lArr; %title', true ); ?>
  </div>

  <div class="col_half col_last tright nobottommargin">
    <?php next_post_link( '%link', '%title &rArr;', true ); ?>
  </div>

</div><!-- .post-navigation end -->
