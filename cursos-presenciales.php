<style>
.portfolio-filter {
  float: none;
  margin: 0 0 80px 0;
}

.portfolio-filter li {
	width: 25% !important;
}

.feature-box.fbox-center .fbox-icon {
  margin: 0 auto 30px;
}

#bebes-y-ninos-icon {
  background-image: url('<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/icono1_gris.png');
  width: 50px;
  height: 100px;
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  margin: auto;
}
.cursos-button:hover #bebes-y-ninos-icon, #bebes-y-ninos-icon:hover {
  background-image: url('<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/icono1.png');
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  margin: auto;
}

#embarazo-icon {
  background-image: url('<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/icono2_gris.png');
  width: 50px;
  height: 100px;
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  margin: auto;
}
.cursos-button:hover #embarazo-icon, #embarazo-icon:hover {
  background-image: url('<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/icono2.png');
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  margin: auto;
}

#educacion-familiar-icon {
  background-image: url('<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/icono3_gris.png');
  width: 50px;
  height: 100px;
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  margin: auto;
}
.cursos-button:hover #educacion-familiar-icon, #educacion-familiar-icon:hover {
  background-image: url('<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/icono3.png');
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  margin: auto;
}

#adultos-icon {
  background-image: url('<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/icono4_gris.png');
  width: 50px;
  height: 100px;
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  margin: auto;
}
.cursos-button:hover #adultos-icon, #adultos-icon:hover {
  background-image: url('<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/icono4.png');
  background-position: center;
  background-repeat: no-repeat;
  background-size: contain;
  margin: auto;
}

@media (max-width: 991px){
  .feature-box.fbox-center .fbox-icon {
    margin-bottom: 0;
  }

	.portfolio-filter li {
		width: 50% !important;
    	padding: 5%;
	}

.portfolio-filter {
  margin-top: -25%;
}
}
</style>
<!-- Portfolio Filter
============================================= -->
<ul id="portfolio-filter" class="portfolio-filter clearfix" data-container="#portfolio">
  
  <li class="portfolio-list">
    <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder" style="text-align:center; width:100%; height:130px;">
      <div class="fbox-icon" style="width:100%; height:100%;">
        <a id="button-byn" class="cursos-button" href="#" data-filter=".cm-bebes-y-ninos" style="border-left:none !important;">
          <i class="i-alt noborder" style="width:100px; height:100px; border:none; margin: auto;">
            <div id="bebes-y-ninos-icon"></div>
          </i>
        </a>
      </div>
      <h3>Bebés y Niños</h3>
    </div>
  </li>
  <li class="portfolio-list">
    <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder" style="text-align:center; width:100%; height:130px;">
      <div class="fbox-icon" style="width:100%; height:100%;">
        <a class="cursos-button" href="#" data-filter=".cm-embarazo" style="border-left:none !important;">
          <i class="i-alt noborder" style="width:100px; height:100px; border:none; margin: auto;">
            <div id="embarazo-icon"></div>
          </i>
        </a>
      </div>
      <h3>Embarazo</h3>
    </div>
  </li>
  <li class="portfolio-list">
    <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder" style="text-align:center; width:100%; height:130px;">
      <div class="fbox-icon" style="width:100%; height:100%;">
        <a class="cursos-button" href="#" data-filter=".cm-educacion-familiar" style="border-left:none !important;">
          <i class="i-alt noborder" style="width:100px; height:100px; border:none; margin: auto;">
            <div id="educacion-familiar-icon"></div>
          </i>
        </a>
      </div>
      <h3>Educación Familiar</h3>
    </div>
  </li>
  <li class="portfolio-list">
    <div class="feature-box fbox-center fbox-light fbox-effect nobottomborder" style="text-align:center; width:100%; height:130px;">
      <div class="fbox-icon" style="width:100%; height:100%;">
        <a class="cursos-button" href="#" data-filter=".cm-adultos" style="border-left:none !important;">
          <i class="i-alt noborder" style="width:100px; height:100px; border:none; margin: auto;">
            <div id="adultos-icon"></div>
          </i>
        </a>
      </div>
      <h3>Adultos</h3>
    </div>
  </li>
</ul><!-- #portfolio-filter end -->

<script>
jQuery(document).ready(function(){
  	jQuery('.cursos-button').first().css('background-color', '#702982');
	setTimeout(function(){
      jQuery('#button-byn').trigger( "click" );
    }, 1000);
});
jQuery(".cursos-button").on('click', function(){
  jQuery(".cursos-button").css("background-color", "#fff");
  jQuery(this).css("background-color", "#702982");
});
</script>

<div id="portfolio" class="portfolio grid-container portfolio-3 clearfix" style="margin: 0 auto;">

  <?php
  global $events;

  $args = array(
    'post_type' => 'post',
    'category_name' => 'bebes-y-ninos, embarazo, educacion-familiar, adultos',
    'posts_per_page' => '100',
	"orderby" => "title",
    'order' => 'ASC'
  );

  $query = new WP_Query($args);

  if($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
    <?php
    $cat = get_the_category(get_the_ID());
    $class_cat_array = "";
    $cat_array = "";
    for ($i = 0; $i < count($cat); $i++){
      $class_cat_array = " cm-" . $cat[$i]->slug . $class_cat_array;
      $aux = ($i >= 1) ? " / " : "";
      $cat_array = $cat_array . $aux . $cat[$i]->name;
    }

	$the_title = get_the_title();

	if(strlen($the_title) < 75){
		$inv_chars = "";

		for($i = 0; $i < (75 - strlen($the_title)); $i++){
			$inv_chars .= " &nbsp;";
		}

		$the_title .= $inv_chars;
	}
    ?>
    <article class="portfolio-item<?php echo $class_cat_array; ?>">
      <div class="portfolio-image">
        <a href="<?php the_permalink(); ?>">
          <img src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="<?php the_title(); ?>">
        </a>
        <div class="portfolio-overlay">
          <a href="<?php the_permalink(); ?>" class="center-icon"><i class="icon-line-ellipsis"></i></a>
        </div>
      </div>
      <div class="portfolio-desc">
        <span><?php echo $cat_array; ?></span>
        <h3><a href="<?php the_permalink(); ?>"><?php echo $the_title;?></a></h3>
      </div>
    </article>

    <?php
    $meta = get_post_meta(get_the_ID(), 'fecha');
    if(!empty($meta)){
      foreach ($meta as $meta_val) {
        $date = $meta_val;
        $e_data["date"] = date("m-d-Y", strtotime($date));
        $e_data["url"] = get_the_permalink();
        $e_data["title"] = get_the_title();

        $events[] = $e_data;
      }
    }
    ?>

  <?php endwhile; endif; ?>

</div>

<script>
jQuery(window).load(function(){
	jQuery('#portfolio').isotope({ filter: '.cm-adultos' });
});
</script>
