<!-- seccion Tips gratuitos de nuestros expertos -->
<style>
.ultimoVideoTipLeft, .ultimoVideoTip, .feature-box > h3 {
  font-size: 20px !important;
}
</style>
<div style="background-color:#f6f5f5;">

  <div class="row">
  <div class="center">
    <h2 class="center nobottommargin topmargin leftmargin-sm rightmargin-sm">Tips gratuitos de nuestros expertos</h2>
	<p style="font-size:20px; margin: auto 35px 20px;">En ésta sección encontrarás tips que te ayudarán a resolver muchas dudas <br>en la crianza de tus hijos según su edad. ¡No te los pierdas!</p>
    <a href="/nuestros-expertos-opinan/" target="_self" class=" button" style="text-align: center;">Ver todos los videos</a>

  </div>

    <div class="col-md-4 col-sm-6 bottommargin">

      <div class="feature-box fbox-right topmargin" data-animate="fadeIn"  data-class-sm="center" data-class-xs="center" data-class-xxs="center">
        <div class="fbox-icon" style="width:50px; height: auto;">

        </div>
        <a href="<?php echo esc_url( add_query_arg( 'playlist', 'PLuUdST3aMmqm8lF4u2NhUwksun5DjQVwx', '/nuestros-expertos-opinan/' ) ); ?>" target="_blank"><h3 style="font-size: 20px !important;"><i class="icon-youtube-play" style="font-size:20px; margin-right:10px; color:#7eb31f;"></i>0 a 6 años</h3></a>

      </div>

      <div class="feature-box fbox-right topmargin" data-animate="fadeIn" data-delay="200"  data-class-sm="center" data-class-xs="center" data-class-xxs="center">
        <div class="fbox-icon" style="width:50px; height: auto;">

        </div>
        <a href="<?php echo esc_url( add_query_arg( 'playlist', 'PLuUdST3aMmqkSMiKcZrcfwk1leSza7l-n', '/nuestros-expertos-opinan/' ) ); ?>"><h3 style="font-size: 20px !important;"><i class="icon-youtube-play" style="font-size:20px; margin-right:10px; color:#7eb31f;"></i>7 a 11 años</h3>

      </div>

    </div>

    <div class="col-md-4 hidden-sm center" data-class-sm="hidden" data-class-xs="hidden" data-class-xxs="hidden">
      <img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/play_youtube.png" alt="nuestros experos opinan">
    </div>

    <div class="col-md-4 col-sm-6">

      <div class="feature-box topmargin" data-animate="fadeIn" style="padding-left: 0; margin-left: 0;" data-class-sm="center" data-class-xs="center" data-class-xxs="center">

        <a href="<?php echo esc_url( add_query_arg( 'playlist', 'PLuUdST3aMmqmh2K_I3zw8XKhXUjKhrdIt', '/nuestros-expertos-opinan/' ) ); ?>"><h3 style="font-size: 20px !important;"><i class="icon-youtube-play" style="font-size:20px; margin-right:10px; color:#7eb31f;"></i>Adolescentes</h3></a>

      </div>

      <div class="feature-box topmargin" data-animate="fadeIn" data-delay="200" style="padding-left: 0; margin-left: 0;"  data-class-sm="center" data-class-xs="center" data-class-xxs="center">

        <a href="<?php echo esc_url( add_query_arg( 'playlist', 'PLuUdST3aMmqlN8Lhmrxo87JydzLxrUmjr', '/nuestros-expertos-opinan/' ) ); ?>"><h3 style="font-size: 20px !important;"><i class="icon-youtube-play" style="font-size:20px; margin-right:10px; color:#7eb31f;"></i>Embarazadas</h3></a>

      </div>

    </div>

    <div class="col-md-4 hidden-sm center" data-class-lg="hidden" data-class-md="hidden">
      <img src="<?php bloginfo('template_directory'); ?>/images/recursosCasaMaestra/play_youtube.png" alt="nuestros experos opinan">
    </div>

  </div>

</div>

<!-- fin sección nuestros expertos opinan -->
