<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
  <?php
  $category_detail=get_the_category(get_the_ID());
  $blog = false;
  foreach($category_detail as $cd){
    if (strtoupper($cd->cat_name) === "BLOG") { $blog = true; }
  }
   ?>
  <?php if(!$blog) { get_template_part( 'entry' ); } else { get_template_part( 'entry', 'blog' ); } ?>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
