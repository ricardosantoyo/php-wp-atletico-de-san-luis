<?php get_header(); ?>
<section id="slider" class="slider-parallax revslider-wrap clearfix">
	<div class="slider-parallax-inner">
		<!--
		#################################
			- THEMEPUNCH BANNER -
		#################################
		-->
		<div class="tp-banner-container">
			<div class="tp-banner" >
				<ul>
					<!-- SLIDE  -->
					<?php
					$args = array(
						'post_per_page' => 3,
						'post_type' => 'post',
						'post_status' => 'publish',
						'category_name' => 'noticias'
					);

					$posts = get_posts($args);

					if($posts):
						foreach($posts as $post):
							setup_postdata( $post );
							?>
							<li class="dark" data-transition="zoomout" data-slotamount="1" data-masterspeed="1500" data-delay="8000" data-saveperformance="off">
								<!-- MAIN IMAGE -->
								<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>"  alt="<?php echo get_the_title(); ?>" data-bgposition="center top" data-scale="cover" data-bgrepeat="no-repeat">
								<!-- LAYERS -->

								<!-- LAYER NR. 2 -->
								<div class="tp-caption customin ltl tp-resizeme revo-slider-emphasis-text nopadding noborder"
		            data-x="center"
		            data-y="center"
		            data-width="['800', '800', '600', '400']"
		            data-fontsize="['64', '64', '56', '56']"
		            data-whitespace="['normal', 'normal', 'normal', 'normal']"
		            data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
		            data-speed="800"
		            data-start="1200"
		            data-easing="easeOutQuad"
		            data-splitin="none"
		            data-splitout="none"
		            data-elementdelay="0.01"
		            data-endelementdelay="0.1"
		            data-endspeed="1000"
		            data-endeasing="Power4.easeIn" style="font-size: 38px; white-space: nowrap;"><?php echo get_the_title(); ?></div>

								<div class="tp-caption customin ltl tp-resizeme"
								data-x="center"
								data-y="center"
								data-voffset="['200','199','199','149']"
								data-customin="x:0;y:150;z:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;s:800;e:Power4.easeOutQuad;"
								data-speed="800"
								data-start="1550"
								data-easing="easeOutQuad"
								data-splitin="none"
								data-splitout="none"
								data-elementdelay="0.01"
								data-endelementdelay="0.1"
								data-endspeed="1000"
								data-endeasing="Power4.easeIn" style="z-index: 3; white-space: nowrap;"><a href="#noticias" data-scrollto="#noticias" class="button button-border button-white button-light button-large button-rounded tright nomargin"><i class="icon-chevron-down"></i></a></div>


							</li>
							<?php
							wp_reset_postdata();
						endforeach;
					endif;
					?>
				</ul>
			</div>
		</div><!-- END REVOLUTION SLIDER -->
	</div>
</section>

<div class="section notopborder nomargin" id="filosofia">

	<div class="container clearfix">

		<div class="col_full nobottommargin col_last">

			<h2 class="asl-font asl-filosofia-title color">Filosofía</h2>

			<div class="row">
				<div class="col-md-4">
					<p class="asl-filosofia-subtitle">
						Misión
					</p>
					<p class="asl-filosofia-p">Fomentar y desarrollar los principios y valores fundamentales del deporte en la sociedad, así como el trabajo en equipo dentro y fuera de la cancha.</p>
				</div>
				<div class="col-md-5">
					<p class="asl-filosofia-subtitle">
						Visión
					</p>
					<p class="asl-filosofia-p">Ser el Club de Futbol Mexicano líder, que se distinga por ser un modelo formativo e incluyente para trabajadores, socios comerciales y aficionados, además de ser una empresa con un alto compromiso social en la ciudad.</p>
				</div>
				<div class="col-md-3">
					<p class="asl-filosofia-subtitle">
						Valores
					</p>
					<p class="asl-filosofia-p">
						Trabajo en Equipo<br />
						Actitud de Servicio<br />
						Excelencia<br />
						Compromiso<br />
						Capacidad de Superación<br />
						Pasión<br />
					</p>
				</div>
			</div>

		</div>

	</div>

</div>

<!-- Primer Equipo -->
<div class="section parallax nomargin notopborder dark" id="primerequipo" style="background: url('<?php bloginfo('template_directory'); ?>/images/secciones/tablas/fondo_jugadores.jpg') no-repeat center center fixed; background-size: cover; padding-top: 100px;" data-stellar-background-ratio="0.2">
	<div class="container clearfix">

		<div class="row">

			<div class="col-md-6 bottommargin">
				<center>
					<h2 class="asl-font">TABLAS</h2>
				</center>

				<div class="tabs tabs-responsive clearfix">

					<ul class="tab-nav clearfix">
						<li><a href="#tab-responsive-1">ASCENSO MX</a></li>
					</ul>

					<div class="tab-container">

						<div class="tab-content clearfix" id="tab-responsive-1">
							<style>
							.table.table-stripped {
								height: 350px;
						    overflow-y: scroll;
						    display: block;
							}
							</style>
							<div class="table-responsive">
								<?php echo do_shortcode("[soccer-info id='354' type='table' /]"); ?>
							</div><!-- /.table-responsive -->

						</div>

					</div>

				</div>
			</div>

			<div class="col-md-6 bottommargin">
				<center>
					<h2 class="asl-font">PRIMER EQUIPO</h2>
				</center>

				<div id="oc-team-list" class="owl-carousel team-carousel carousel-widget" data-margin="30" data-nav="true" data-dots="false" data-items-sm="1" data-items-lg="1">

					<?php
					$posts = get_posts(array(
						'numberposts'	=> -1,
						'post_type'		=> 'post',
						'post_status' => 'publish',
						'meta_key'		=> 'primer_equipo',
						'meta_value'	=> '1'
					));

					if($posts):
						foreach( $posts as $post ):

							setup_postdata( $post );
							?>
							<div class="oc-item">
								<center>
									<img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>" class="team-pic" alt="<?php echo get_the_title(); ?>">
								</center>
								<div class="team team-list clearfix">
									<div class="team-desc">
										<center>
											<div class="team-title"><h4><?php the_title(); ?></h4><span><?php the_field('posicion'); ?></span></div>
											<?php if(get_field('facebook-jugador') != '') { ?>
												<a href="https://facebook.com/<?php echo get_field('facebook-jugador') ?>" class="social-icon si-rounded si-small si-facebook center">
													<i class="icon-facebook"></i>
													<i class="icon-facebook"></i>
												</a>
											<?php } ?>
											<?php if(get_field('twitter-jugador') != '') { ?>
												<a href="https://twitter.com/<?php echo get_field('twitter-jugador') ?>" class="social-icon si-rounded si-small si-facebook center">
													<i class="icon-facebook"></i>
													<i class="icon-facebook"></i>
												</a>
											<?php } ?>
										</center>
									</div>
								</div>
							</div>
							<?php
							wp_reset_postdata();
						endforeach;
					endif;
					?>

				</div>
			</div>

		</div>

	</div>
</div>

<!-- Noticias -->
<div class="section notopborder nomargin" id="noticias">

	<div class="container clearfix">

		<div class="col_full nobottommargin col_last">

			<center>
				<h2 class="asl-font asl-noticias-title color">NOTICIAS</h2>
			</center>

			<div class="row">
				<div class="col_one_third">

					<?php
					$args = array(
						'posts_per_page'	=> 2,
						'post_type'		=> 'post',
						'post_status' => 'publish',
						'category_name' => 'noticias'
					);

					$posts = get_posts($args);

					if($posts):
						foreach ($posts as $post) {
							setup_postdata( $post );
							?>
							<div class="entry clearfix">
								<div class="entry-image">
									<div class="fslider" data-arrows="false">
										<div class="flexslider">
											<div class="slider-wrap">
												<div class="slide"><a href="<?php echo get_the_permalink(); ?>"><img class="image_fade" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt="<?php echo get_the_title(); ?>" class="img-clip"></a></div>
											</div>
										</div>
									</div>
								</div>
								<div class="entry-title">
									<h2><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
								</div>
								<ul class="entry-meta clearfix">
									<li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
									<?php
									$comments_count = wp_count_comments($post->ID);
									?>
									<li><i class="icon-comments"></i> <?php echo $comments_count->approved; ?></li>
								</ul>
							</div>
							<?php
							wp_reset_postdata();
						}
					endif;
					?>
				</div>
				<div class="col_one_third">
					<?php
					$args = array(
						'posts_per_page'	=> 1,
						'offset' => 2,
						'post_type'		=> 'post',
						'post_status' => 'publish',
						'category_name' => 'noticias'
					);

					$posts = get_posts($args);

					if($posts):
						foreach ($posts as $post) {
							setup_postdata( $post );
							?>
							<div class="entry clearfix">
								<div class="entry-image">
									<a href="<?php echo get_the_permalink(); ?>"><img class="image_fade" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt="<?php echo get_the_title(); ?>"></a>
								</div>
								<div class="entry-title">
									<h2><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
								</div>
								<ul class="entry-meta clearfix">
									<li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
									<?php
									$comments_count = wp_count_comments($post->ID);
									?>
									<li><i class="icon-comments"></i> <?php echo $comments_count->approved; ?></li>
								</ul>
							</div>
							<?php
							wp_reset_postdata();
						}
					endif;
					?>

					<div class="row">
						<?php
						$args = array(
							'posts_per_page'	=> 2,
							'offset' => 3,
							'post_type'		=> 'post',
							'post_status' => 'publish',
							'category_name' => 'noticias'
						);

						$posts = get_posts($args);

						if($posts):
							foreach ($posts as $post) {
								setup_postdata( $post );
								?>
								<div class="spost clearfix">
									<div class="entry-image">
										<a href="<?php echo get_the_permalink(); ?>" class="nobg"><img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt=""></a>
									</div>
									<div class="entry-c">
										<div class="entry-title">
											<h4><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h4>
										</div>
										<ul class="entry-meta">
											<li><?php echo get_the_date(); ?></li>
										</ul>
									</div>
								</div>
								<?php
								wp_reset_postdata();
							}
						endif;
						?>
					</div>
				</div>
				<div class="col_one_third col_last">
					<?php
					$args = array(
						'posts_per_page'	=> 1,
						'offset' => 5,
						'post_type'		=> 'post',
						'post_status' => 'publish',
						'category_name' => 'noticias'
					);

					$posts = get_posts($args);

					if($posts):
						foreach ($posts as $post) {
							setup_postdata( $post );
							?>
							<div class="entry clearfix">
								<div class="entry-image">
									<a href="<?php echo get_the_permalink(); ?>"><img class="image_fade" src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt="<?php echo get_the_title(); ?>"></a>
								</div>
								<div class="entry-title">
									<h2><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
								</div>
								<ul class="entry-meta clearfix">
									<li><i class="icon-calendar3"></i> <?php echo get_the_date(); ?></li>
									<?php
									$comments_count = wp_count_comments($post->ID);
									?>
									<li><i class="icon-comments"></i> <?php echo $comments_count->approved; ?></li>
								</ul>
							</div>
							<?php
							wp_reset_postdata();
						}
					endif;
					?>

					<div class="row">
						<a href="/noticias">
							<img src="<?php bloginfo('template_directory'); ?>/images/noticias.png" class="img-responsive" />
						</a>
					</div>
				</div>
			</div>

		</div>

	</div>

</div>

<!-- Academia -->
<div class="section parallax nomargin notopborder dark" id="academia" style="background: url('<?php bloginfo('template_directory'); ?>/images/secciones/academia/fondoacademia.png') no-repeat center center fixed; padding-top: 100px;" data-stellar-background-ratio="0.3">
	<div class="container clearfix">

		<center>
			<h2 class="asl-font asl-academia-title">ACADEMIA</h2>
		</center>

		<div class="row">
			<div class="col_one_third">
				<h3 class="asl-academia-subtitle">Inscripción</h3><span class="asl-academia-subtitle"><i>desde</i></span>
				<h2 class="asl-academia-price"><span>$</span>1995</h2>
				<br />
				<h3 class="asl-academia-subtitle">Mensualidad</h3><span class="asl-academia-subtitle"><i>desde</i></span>
				<h2 class="asl-academia-price"><span>$</span>750</h2>
			</div>
			<div class="col_one_third">
				<ul class="iconlist asl-academia-list">
					<li><i class="icon-checkmark"></i> Ropa Nike</li>
					<li><i class="icon-checkmark"></i> Competencias en ligas</li>
					<li><i class="icon-checkmark"></i> Entrenamientos en la ciudad deportiva la presa</li>
					<li><i class="icon-checkmark"></i> Metodología del Atlético de Madrid</li>
					<li><i class="icon-checkmark"></i> Seguro Médico</li>
					<li><i class="icon-checkmark"></i> Participación en ligas</li>
					<li><i class="icon-checkmark"></i> Transportes a partidos de visita</li>
					<li><i class="icon-checkmark"></i> Arbitrajes</li>
				</ul>
			</div>
			<div class="col_one_third col_last">
				<ul class="iconlist asl-academia-list">
					<li><i class="icon-checkmark"></i> Hidratación en entrenamientos y partidos</li>
					<li><i class="icon-checkmark"></i> Credencial</li>
					<li><i class="icon-checkmark"></i> Portal informático GESDEP</li>
					<li><i class="icon-checkmark"></i> Servicios médicos</li>
					<li><i class="icon-checkmark"></i> Nutriólogo</li>
					<li><i class="icon-checkmark"></i> Psicólogo</li>
					<li><i class="icon-checkmark"></i> Uso de material deportivo</li>
					<li><i class="icon-checkmark"></i> Reporte mensual de avance</li>
				</ul>

				<center>
					<a href="mailto:academia@atleticodesanluis.mx" class="button button-xlarge button-border-thin button-rounded tright asl-academia-button">INSCRIPCIONES</a>
				</center>
			</div>
		</div>

	</div>
</div>

<!-- Boletos -->
<div class="section notopborder nomargin nopadding" id="boletos">

	<div class="container clearfix">

		<div class="col_full nobottommargin col_last">

			<div class="row">
				<div class="col_half">
					<div class="notopborder nomargin" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/secciones/boletos/fondo_boletos.png'); background-size: cover; padding-top: 100px;" id="boletos-div">
						<center>
							<h2 class="asl-boletos-title">ASEGURA TU LUGAR<span>en todos los partidos del</span><span class="asl-boletos-span">Atlético de San Luis</span></h2>
						</center>

						<div class="row">
							<div class="col_one_fourth">

							</div>
							<div class="col_three_fourth col_last">
								<blockquote class="asl-boletos-blockquote">
									<p>
										EN LA LIGA<br />
										DE ASCENSO<br />
										<span>MX</span>
									</p>
								</blockquote>
							</div>
						</div>
					</div>
				</div>
				<div class="col_half col_last">
					<div class="row">
						<div class="section nomargin" style="padding: 100px 0;">
							<div class="col-md-6 pricing-box-set">
								<div class="pricing-box">
									<div class="pricing-title">
										<h3 class="asl-font color asl-boletos-pricing-title">BOLETOS</h3>
									</div>
									<div class="pricing-features">
										<div class="asl-boletos-pricing-list">
											<p>
												<div class="row">
													<div class="fleft">
														General A
													</div>
													<div class="fright">
														<small>$</small>2100<small>MXN</small>
													</div>
												</div>
											</p>
											<p>
												<div class="row">
													<div class="fleft">
														General B
													</div>
													<div class="fright">
														<small>$</small>2200<small>MXN</small>
													</div>
												</div>
											</p>
											<p>
												<div class="row">
													<div class="fleft">
														Cabecera Nte
													</div>
													<div class="fright">
														<small>$</small>2900<small>MXN</small>
													</div>
												</div>
											</p>
											<p>
												<div class="row">
													<div class="fleft">
														Cabecera Sur
													</div>
													<div class="fright">
														<small>$</small>3100<small>MXN</small>
													</div>
												</div>
											</p>
										</div>

										<div class="divider divider-short divider-center">
											<i class="icon-asterisk"></i>
										</div>

										<p>
											Disponibles en la Taquilla del Estadio
										</p>
									</div>
									<div class="pricing-action">
										<a href="http://web.superboletos.com:8001/SuperBoletos/index.do" target="_blank" class="btn btn-danger btn-block btn-lg">Comprar en línea</a>
									</div>
								</div>
							</div>
							<div class="col-md-6 pricing-box-set">
								<div class="pricing-box best-price">
									<div class="pricing-title">
										<h3 class="asl-font color asl-boletos-pricing-title">ABONOS</h3>
									</div>
									<div class="pricing-features">
										<div class="asl-boletos-pricing-list">
											<p>
												<div class="row">
													<div class="fleft">
														General A
													</div>
													<div class="fright">
														<small>$</small>2100<small>MXN</small>
													</div>
												</div>
											</p>
											<p>
												<div class="row">
													<div class="fleft">
														General B
													</div>
													<div class="fright">
														<small>$</small>2200<small>MXN</small>
													</div>
												</div>
											</p>
											<p>
												<div class="row">
													<div class="fleft">
														Cabecera Nte
													</div>
													<div class="fright">
														<small>$</small>2900<small>MXN</small>
													</div>
												</div>
											</p>
											<p>
												<div class="row">
													<div class="fleft">
														Cabecera Sur
													</div>
													<div class="fright">
														<small>$</small>3100<small>MXN</small>
													</div>
												</div>
											</p>
										</div>

										<div class="divider divider-short divider-center">
											<i class="icon-asterisk"></i>
										</div>

										<p>
											Disponibles en la Taquilla del Estadio
										</p>
									</div>
									<div class="pricing-action">
										<a href="http://web.superboletos.com:8001/SuperBoletos/index.do" target="_blank" class="btn btn-danger btn-block btn-lg">Comprar en línea</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

	</div>

</div>

<!-- Tienda -->
<div class="section parallax nomargin notopborder dark" id="tienda" style="background: url('<?php bloginfo('template_directory'); ?>/images/secciones/tienda/fondo_pasto.jpg') repeat center center fixed; padding-top: 100px; padding-bottom: 0 !important;" data-stellar-background-ratio="0.3">
	<div class="container clearfix">

		<div class="row">
			<div class="col_half" style="margin-bottom: 0;">
				<h1 class="asl-tienda-title">ADQUIERE<br />TU JERSEY OFICIAL</h1>

				<div class="row">
					<div class="col_half" style="margin-bottom: 0;">
						<img src="<?php bloginfo('template_directory'); ?>/images/secciones/tienda/jersey.png" class="img-responsive" data-animate="fadeInLeft" data-delay="500" />
					</div>
					<div class="col_half col_last">
						<center>
							<br />
							<h2 style="font-family: 'ASL'; color: #eee;">¡Consíguelo antes de que se acabe!</h2>
							<h4 style="font-family: 'Lato'; color: #eee;">Encuéntranos en el Estadio Alfonso Lastras</h4>
						</center>
					</div>
				</div>
			</div>
			<div class="col_half col_last">
				<img src="<?php bloginfo('template_directory'); ?>/images/secciones/tienda/jugador_VERDE.png" class="img-responsive" data-animate="fadeInRight" data-delay="500" />
			</div>
		</div>

	</div>
</div>

<!-- Responsabilidad Social -->
<div class="section parallax nomargin notopborder" id="responsabilidad" style="background: url('<?php bloginfo('template_directory'); ?>/images/secciones/responsabilidad/FONDO_POSTOSINOSENACCION.jpg') no-repeat center center fixed; padding-top: 100px;" data-stellar-background-ratio="0.3">
	<div class="container clearfix">

		<div class="row">
			<div class="col_two_third" style="margin-bottom: 0;">
				<center>
					<img src="<?php bloginfo('template_directory'); ?>/images/secciones/responsabilidad/potosinos.png" class="img-responsive" />
				</center>

				<br />

				<div class="owl-carousel portfolio-carousel carousel-widget" id="asl-responsabilidad-carousel" data-margin="30" data-nav="false" data-pagi="true" data-autoplay="5000" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-lg="3">

					<?php
					$args = array(
						'post_per_page' => 6,
						'post_type' => 'post',
						'post_status' => 'publish',
						'category_name' => 'responsabilidad-social'
					);

					$posts = get_posts($args);

					if($posts):
						foreach($posts as $post):
							setup_postdata( $post );
							?>
							<div class="oc-item">
								<div class="iportfolio">
									<div class="portfolio-image">
										<a href="<?php echo get_the_permalink(); ?>">
											<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt="<?php echo get_the_title(); ?>">
										</a>
									</div>
									<div class="portfolio-desc">
										<h3><?php echo get_the_title(); ?></h3>
									</div>
								</div>
							</div>
							<?php
							wp_reset_postdata();
						endforeach;
					endif;
					?>

				</div><!-- .portfolio-carousel end -->
			</div>
			<div class="col_one_third col_last">
				<div id="contact-form-overlay-mini" class="clearfix">
					<div class="fancy-title">
						<h3>Contáctanos</h3>

						<div class="contact-widget">

							<div class="contact-form-result"></div>

							<!-- Contact Form
							============================================= -->
							<form class="nobottommargin" id="template-contactform" name="template-contactform" action="<?php bloginfo('template_directory'); ?>/include/sendemail.php" method="post">

								<div class="col_half">
									<label for="template-contactform-name">Nombre <small>*</small></label>
									<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required" />
								</div>

								<div class="col_half col_last">
									<label for="template-contactform-email">Correo <small>*</small></label>
									<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control" />
								</div>

								<div class="clear"></div>

								<div class="col_full">
									<label for="template-contactform-phone">Teléfono</label>
									<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control" />
								</div>

								<div class="clear"></div>

								<div class="col_full">
									<label for="template-contactform-message">Mensaje <small>*</small></label>
									<textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
								</div>

								<div class="col_full hidden">
									<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
								</div>

								<div class="col_full">
									<center>
										<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Enviar</button>
									</center>
								</div>

							</form>

						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- Zona Fan -->
<div class="section notopborder nomargin nopadding" id="zona-fan">

	<div class="container clearfix">

		<div class="col_full nobottommargin col_last">

			<div class="row">
				<div class="col_full col_last" style="margin-right: 0;">
					<div class="asl-zona-fan">
						<h2 class="asl-font asl-zona-fan-title">Zona Fan</h2>
						<h2 class="asl-zona-fan-subtitle">¿Cuánto sabes del <br /><strong><span class="asl-boletos-span">Atlético de San Luis</span></strong>?</h2>
					</div>

					<center>
						<iframe src="/quiz.php" style="width: 100%; height: 500px;" id="asl-quiz"></iframe>
					</center>

				</div>
				<div class="col_full col_last">

					<div id="portfolio" class="portfolio grid-container grid-3 portfolio-nomargin portfolio-full portfolio-masonry mixed-masonry grid-container clearfix">

						<?php
						$args = array(
							'post_per_page' => 6,
							'post_type' => 'post',
							'post_status' => 'publish',
							'category_name' => 'fondos-de-pantalla'
						);

						$posts = get_posts($args);

						if($posts):
							foreach($posts as $post):
								setup_postdata( $post );
								?>
								<article class="portfolio-item pf-media pf-icons wide">
									<div class="portfolio-image">
										<img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" alt="<?php echo get_the_title(); ?>">
										<div class="portfolio-overlay">
											<a href="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" download="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" class="center-icon"><i class="icon-download2"></i></a>
										</div>
									</div>
								</article>
								<?php
								wp_reset_postdata();
							endforeach;
						endif;
						?>
					</div>

					<div class="center">
						<iframe src="https://open.spotify.com/embed/user/n1paaiddscq8oxhjnyz4h6lhw/playlist/0pQ3Bd82KKezEmhGcepvLy" width="100%" height="280" frameborder="0" allowtransparency="true"></iframe>
					</div>
				</div>
			</div>

		</div>

	</div>

</div>

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap" style="padding-top:0 !important;">

					<?php
					/*if ( is_front_page() || is_home() ) {
						$offset = (get_query_var('paged')) ? get_query_var('paged') : 0;
						get_home_posts($offset);
						?>
						<div class="col_half nobottommargin">
							<?php next_posts_link( '&lArr; Entradas Anteriores' ); ?>
						</div>
						<div class="col_half col_last tright nobottommargin">
							<?php previous_posts_link( 'Nuevas Entradas &rArr;' ); ?>
						</div>
						<?php
					}*/
					/*
					if ( is_front_page() || is_home() ) {

						//We get the main content tabs
						cm_get_tabs();

					}*/
					?>

			</div>

			<?php
			/*
			if ( is_front_page() || is_home() ) {

				get_template_part( 'main', 'tips' );
				get_template_part( 'main', 'espacios' );
				cm_blog_index_func();
				get_template_part( 'main', 'newsletter' );

			} */?>

		</section><!-- #content end -->

		<?php get_footer(); ?>
