<style type="text/css">

		a {
			color: #7D499B;
		}

		a:hover {
			font-weight: bolder;
			color: #7D499B;
		}
		p{
			text-align: justify;
		}

		h4 { color: #7D499B; }
	</style>
  <p>Al ingresar y utilizar este portal de Internet, cuyo nombre de dominio es <a href="index.html" target="_self">www.casamaestra.com.mx </a>propiedad de Casa Maestra Online SA de CV (en adelante, Casa Maestra), el usuario está aceptando los Términos y condiciones de uso contenidos en este convenio y declara expresamente su aceptación utilizando para tal efecto medios electrónicos, en términos de lo dispuesto por el artículo 1803 del Código Civil Federal.</p>

					<p>En caso de no aceptar en forma absoluta y completa los términos y condiciones de este convenio, el usuario deberá abstenerse de acceder, utilizar y observar el sitio web <a href="index.html" target="_self">www.casamaestra.com.mx</a> </p>


					<p>Y en caso de que el usuario acceda, utilice y observe el sitio web <a href="index.html" target="_self">www.casamaestra.com.mx</a> se considerará como una absoluta y expresa aceptación de los Términos y condiciones de uso aquí estipulados.</p>

					<p>La sola utilización de dicha página de Internet le otorga al público en general la condición de usuario (en adelante referido como el «usuario» o los «usuarios») e implica la aceptación, plena e incondicional, de todas y cada una de las condiciones generales y particulares incluidas en estos Términos y condiciones de uso publicados por <a href="index.html" target="_self">www.casamaestra.com.mx </a>  en el momento mismo en que el usuario acceda al sitio web.</p>

					<p>Cualquier modificación a los presentes Términos y condiciones de uso será realizada cuando el titular de la misma, en este caso Casa Maestra , lo considere apropiado, siendo exclusiva responsabilidad del usuario asegurarse de tomar conocimiento de tales modificaciones.</p>


					<h4>CONVENIO</h4>

					<p>Convenio de adhesión para el uso de la página de Internet <a href="index.html" target="_self">www.casamaestra.com.mx </a> que celebran: por una parte Casa Maestra y por la otra, el usuario, sujetándose, ambas partes, a lo establecido en este documento. </p>

					<h4>LICENCIA</h4>

					<p>Por virtud de la celebración de este convenio, Casa Maestra otorga y concede al usuario el derecho no exclusivo, revocable y no transferible de ver y utilizar el sitio web <a href="index.html" target="_self">www.casamaestra.com.mx </a> de conformidad con los Términos y condiciones de uso que aquí se estipulan. Para los efectos del presente convenio, las partes acuerdan que por «usuario» se entenderá a cualquier persona de cualquier naturaleza que ingrese al sitio web  www.casamaestra.com.mx  y/o a cualquiera de las subpáginas que despliegan su contenido y/o a la persona de cualquier naturaleza que se dé de alta y/o use cualquiera de los servicios que se ofrecen a través de dicha página. </p>

					<p>El usuario sólo podrá imprimir y/o copiar cualquier información contenida o publicada en el sitio web <a href="index.html" target="_self">www.casamaestra.com.mx </a> exclusivamente para uso personal, queda terminantemente prohibido el uso comercial de dicha información. En caso de ser persona moral se sujetará a lo dispuesto por el artículo 148, fracción IV de la Ley Federal del Derecho de Autor.</p>

					<p>La reimpresión, publicación, distribución, asignación, sublicencia, venta, reproducción electrónica o por otro medio, parcial o total, de cualquier información, documento o gráfico que aparezca en el sitio web <a href="index.html" target="_self">www.casamaestra.com.mx</a>, para cualquier uso distinto al personal o comercial le está expresamente prohibido al usuario, a menos de que cuente con la autorización previa y por escrito de Casa Maestra</p>

					<h4>PAUTAS PARA USO DE SITIO WEB <a href="index.html" target="_self">www.casamaestra.com.mx</a></h4>


					<p>El usuario y  Casa Maestra están de acuerdo en que la utilización del sitio web <a href="index.html" target="_self"> www.casamaestra.com.mx </a> se sujetará a las siguientes reglas: </p>

					<p>Información contenida en el sitio web <a href="index.html" target="_self"> www.casamaestra.com.mx</a>.</p>

					<p>El usuario reconoce y acepta que la información publicada o contenida en dicho sitio será claramente identificada de forma tal que se reconozca que la misma proviene y ha sido generada por Casa Maestra. <br>
Si el usuario desea obtener mayor información de un tema en específico proveído por Casa Maestra deberá solicitarlo directamente en los datos de contacto.</p>



					<h4>RENTA DE CURSOS ONLINE</h4>

					<p>Una vez pagado el curso el usuario tendrá derecho a la visualización del curso rentado por tres meses a través del sistema de Casa Maestra, pasado ese tiempo el usuario deberá de pagar de nuevo el curso para poderlo visualizar. El pago de cada curso es por separado.</p>


					<h4>CURSOS PRESENCIALES</h4>

					<p>El costo de cada curso se anuncia en la página específica del mismo. Así como horarios y especificaciones. <br>
Los usuarios reconocen que, al proporcionar la información de carácter personal requerida en alguno de los servicios que se prestan en este sitio web, otorgan a Casa Maestra la autorización señalada en el artículo 109 de la Ley Federal del Derecho de Autor. En todos los casos, los usuarios responderán por la veracidad de la información proporcionada a Casa Maestra. Asimismo, para la prestación de servicios, el usuario se obliga además a aceptar los términos y condiciones estipulados para tal efecto.</p>

					<h4>DERECHOS DE AUTOR Y PROPIEDAD INDUSTRIAL</h4>

					<p>Casa Maestra el sitio web <a href="index.html" target="_self">www.casamaestra.com.mx</a>, sus logotipos y todo el material que aparece en dicho sitio, son marcas, nombres de dominio, nombres comerciales y obras artísticas propiedad de sus respectivos titulares y están protegidos por los tratados internacionales y las leyes aplicables en materia de propiedad intelectual y derechos de autor. </p>


					<p>Los derechos de autor sobre el contenido, organización, recopilación, compilación, información, logotipos, fotografías, imágenes, programas, aplicaciones, y en general cualquier información contenida o publicada en el sitio web <a href="index.html" target="_self">www.casamaestra.com.mx </a>  se encuentran debidamente protegidos a favor de Casa Maestra sus afiliados, proveedores y/o de sus respectivos propietarios, de conformidad con la legislación aplicable en materia de propiedad intelectual e industrial.</p>

					<p>Se prohíbe expresamente al usuario modificar, alterar o suprimir, ya sea en forma total o parcial, los avisos, marcas, nombres comerciales, señas, anuncios, logotipos o en general cualquier indicación que se refiera a la propiedad de la información contenida en el sitio señalado. <br>
Es nuestra política actuar contra las violaciones que en materia de propiedad intelectual se pudieran generar u originar según lo estipulado en la legislación y en otras leyes de propiedad intelectual aplicables, incluyendo la eliminación o el bloqueo del acceso a material que se encuentra sujeto a actividades que infrinjan el derecho de propiedad intelectual de terceros.</p>


					<p>En caso de que algún usuario o tercero consideren que cualquiera de los contenidos que se encuentren o sean introducidos en dicho sitio <a href="index.html" target="_self">http://www.casamaestra.com.mx </a> y/o cualquiera de sus servicios, violen sus derechos de propiedad intelectual deberán enviar una notificación a la siguiente dirección <a href="mailto:info@casamaestra.com.mx"> info@casamaestra.com.mx </a> en la que indiquen:</p>

					<p>I.Datos personales verídicos (nombre, dirección, número de teléfono y dirección de correo electrónico del reclamante); <br>

					II. Firma autógrafa con los datos personales del titular de los derechos de propiedad intelectual;<br>

					III. Indicación precisa y completa del (los) contenido (s) protegido (s) mediante los derechos de propiedad intelectual supuestamente infringidos, así como la localización de dichas violaciones en el sitio web referido; <br>

					IV. Declaración expresa y clara de que la introducción del (los) contenido (s) indicado (s) se ha realizado sin el consentimiento del titular de los derechos de propiedad intelectual supuestamente infringidos;<br>

					V. Declaración expresa, clara y bajo la responsabilidad del reclamante de que la información proporcionada en la notificación es exacta y de que la introducción del (los) contenido (s) constituye una violación de dichos derechos.</p>

					<h4>MATERIAL Y PUBLICIDAD</h4>

					<p>El usuario reconoce y acepta que es una organización independiente de terceros patrocinadores y anunciantes cuya información, imágenes, anuncios y demás material publicitario o promocional (en lo subsecuente «material publicitario›) puede ser publicado en el sitio web
					http:// <a href="index.html" target="_self">www.casamaestra.com.mx</a></p>

					<p>El usuario reconoce y acepta que el material publicitario no forma parte del contenido principal que se publica en dicho sitio. Asimismo, reconoce y acepta con este acto que este material se encuentra protegido por las leyes que en materia de propiedad intelectual e industrial resulten aplicables.<br>
					Casa Maestra se libera de cualquier responsabilidad y condiciones, tanto expresas como implícitas, en relación con los servicios e información contenida o disponible en o a través de esta página web; incluyendo, sin limitación alguna:
</p>

<p>La disponibilidad de uso del sitio web http://
<a href="index.html">www.casamaestra.com.mx</a>
La ausencia de virus, errores, desactivadores o cualquier otro material contaminante o con funciones destructivas en la información o programas disponibles en o a través de esta página o en general cualquier falla en dicho sitio. <br>
No obstante lo anterior, Casa Maestra actualizará el contenido de la página constantemente, por lo que se solicita al usuario tomar en cuenta que algunas informaciones publicitadas o contenidas en o a través de este sitio web pueden haber quedado obsoletas y/o contener imprecisiones o errores tipográficos u ortográficos.</p>


<h4>LIMITACIONES A LA RESPONSABILIDAD</h4>

<p>Hasta el máximo permitido por las leyes aplicables, Casa Maestra no será responsable, en ningún caso, por daños directos, especiales, incidentales, indirectos, o consecuenciales que en cualquier forma se deriven o se relacionen con: <br>
El uso o ejecución del sitio web http://
<a href="index.html" target="_self">www.casamaestra.com.mx </a>, con el retraso o la falta de disponibilidad de uso de Casa Maestra
La proveeduría o falta de la misma de servicios de cualquier información o gráficos contenidos o publicados en o a través del sitio señalado.
La actualización o falta de actualización de la información.
La alteración o modificación, total o parcial, de la información después de haber sido incluida en dicho sitio. <br>
Cualquier otro aspecto o característica de la información contenida o publicada en la página web o a través de las ligas que eventualmente se incluyan en este sitio.
La proveeduría o falta de proveeduría que los demás servicios, todos los supuestos anteriores serán vigentes, aún en los casos en que se le hubiere notificado o avisado a Casa Maestra  acerca de la posibilidad de que se ocasionaran dichos daños.
Modificaciones al sitio web http://
<a href="index.html">www.casamaestra.com.mx</a></p>

<p>Casa Maestra podrá en cualquier momento y cuando lo considere conveniente, sin necesidad de avisar al usuario, realizar correcciones, adiciones, mejoras o modificaciones al contenido, presentación, información, servicios, áreas, bases de datos y demás elementos de dicho sitio, sin que ello de lugar ni derecho a ninguna reclamación o indemnización, ni que esto implique reconocimiento de responsabilidad alguna a favor del usuario.</p>


<h4>MODIFICACIONES AL CONVENIO </h4>

<p>Casa Maestra se reserva el derecho de modificar los Términos y condiciones de uso de este convenio en cualquier momento, siendo efectivas dichas modificaciones de forma inmediata por medio de: <br>
La publicación en el sitio web http://<a href="index.html">www.casamaestra.com.mx </a>del convenio modificado.
La notificación al usuario sobre dichas modificaciones. <br>

De esta forma, el usuario está de acuerdo en revisar dicho convenio periódicamente con la finalidad de mantenerse al tanto de dichas modificaciones. No obstante lo anterior, cada vez que el usuario acceda al sitio señalado se considerará como una aceptación absoluta a las modificaciones del presente convenio.

</p>

<h4>TÉRMINOS ADICIONALES </h4>

<p>Ocasionalmente, Casa Maestra  podrá agregar a los Términos y condiciones de uso del presente convenio provisiones adicionales relativas a áreas específicas o nuevos servicios que se proporcionen en o a través del sitio web http://
<a href="index.html" target="_self">www.casamestra.com.mx </a> (en lo subsecuente «términos adicionales»), los cuales serán publicados en las áreas específicas o nuevos servicios de dicho sitio para su lectura y aceptación. El usuario reconoce y acepta que estos términos adicionales forman parte integrante del presente convenio para todos los efectos legales a que haya lugar. </p>

<h4>INDEMNIZACIÓN</h4>
<p>El usuario está de acuerdo en indemnizar a Casa Maestra, sus afiliados, proveedores, vendedores y asesores por cualquier acción, demanda o reclamación (incluso de honorarios de abogados y de costas judiciales) derivadas de cualquier incumplimiento por parte del usuario al presente convenio; incluyendo, sin limitación de alguna de las derivadas de: <br>
Cualquier aspecto relativo al uso del sitio web http://<a href="index.html" target="_self">www.casamaestra.com.mx</a>
La información contenida o disponible en o a través de dicho sitio o de injurias, difamación o cualquier otra conducta violatoria del presente convenio por parte del usuario en el uso de la página web señalada.
La violación a las leyes aplicables o tratados internacionales relativos a los derechos de autor o propiedad intelectual, contenidos o disponibles en, o a través de dicho sitio web.</p>



<div style="margin-bottom: 30px; padding:15px;">

<h4>FORMAS DE PAGO</h4>

<div class="col_half" style="border:1px solid #515151; padding:15px;">


<h4 style="background-color:#7D499B; padding:10px; color:#fff; ">Pago para cursos Online</h4>
<p>El pago de cada curso será único. <br>
El pago de los cursos online podrá realizarse por transferencia o en línea por medio de Paypal o Conekta.</p>
</div>

<div class="col_half col_last" style="border:1px solid #515151; padding:15px;">


<h4 style="background-color: #8EC746; padding:10px; color:#fff;">Pago para cursos Presenciales</h4>

<p>Los cursos presenciales pueden pagarse por Paypal, transferencia, efectivo o tarjeta de crédito en la sucursal de Casa Maestra.</p>

</div>


</div>

<div class="clear"></div>




<p>En caso de realizar el pago por transferencia electrónica o depósito bancario, deberá mandar el comprobante por mail a <a href="mailto:info@casamaestra.com.mx" target="_blank">info@casamaestra.com.mx</a>  con todos sus datos (día de depósito, cantidad, nombre, teléfono y  nombre de la empresa).</p>

<p>Pago con Transferencia bancaria: debe poner como referencia alfanumérica su nombre completo o el de su empresa. <br>
Los datos de transferencia son los siguientes:<br>
Casa Maestra Polanco SC <br>
Banco: Banorte<br>
Clabe interbancaria: 072180000146177732<br>
Los precios de todas las opciones se encuentran dentro del portal en cada uno de los cursos y podrán cambiar sin previo aviso.</p>


<h4>TERMINACIÓN</h4>

<p>Casa Maestra se reserva el derecho, a su exclusiva discreción, y sin necesidad de aviso o notificación al usuario, para: <br>
Terminar definitivamente el presente convenio.
Descontinuar o dejar de publicar definitivamente el sitio web www.casamestra.com.mx sin responsabilidad alguna para Casa Maestra, sus afiliados o proveedores. <br>
En caso de que el cliente quiera cancelar su cuenta por causas ajenas a Casa Maestra no se devolverá ningún porcentaje del dinero pagado. En caso de que exista algún motivo de cancelación imputable a Casa Maestra, la devolución del dinero será revisada en cada caso en particular y se definirá la manera de devolverlo al cliente.</p>


<h4>SUBSISTENCIA</h4>

<p>Estos Términos y condiciones de uso, así como los términos adicionales, constituyen el acuerdo íntegro entre las partes, y sustituye cualquier otro acuerdo o convenio celebrado con anterioridad. Cualquier cláusula o provisión del presente convenio, así como de los términos adicionales, legalmente declarada inválida, será eliminada o modificada a elección de Casa Maestra, con la finalidad de corregir su vicio o defecto. Sin embargo, el resto de las cláusulas o provisiones mantendrán su fuerza, obligatoriedad y validez.
</p>

<h4>NO RENUNCIA DE DERECHOS</h4>

<p>La inactividad por parte de Casa Maestra sus afiliados o proveedores al ejercicio de cualquier derecho o acción derivados del presente convenio, en ningún momento deberá interpretarse como renuncia a dichos derechos o acciones.</p>

<h4>LEGISLACIÓN APLICABLE Y JURISDICCIÓN</h4>
<p>Este convenio estará sujeto y será interpretado de acuerdo con las leyes y ante los tribunales del Distrito Federal, México.</p>
