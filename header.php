<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="Ricardo Santoyo Reyes" />
  <meta name="keywords" content="futbol, atlético de san luis, atlético, liga mx, copa mx, ascenso mx, fútbol, soccer, deporte, atlético de madrid, san luis potosí">
  <meta name="description" content="Sitio oficial del club Atlético de San Luis">
  <?php
  if(is_front_page()){
    $default_image="http://atleticodesanluis.mx/wp-content/uploads/2017/12/openGraph_atletico.png";
    echo '<meta property="og:image" content="' . $default_image . '"/>';
  }
  ?>

  <!-- Stylesheets
	============================================= -->
	<link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700,800,900|Roboto:400,100,300,500,700,900|Permanent+Marker" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/style.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/dark.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/font-icons.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/animate.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/magnific-popup.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/main.css" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/colors.css" type="text/css" />

  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/components/radio-checkbox.css" type="text/css" />

  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/responsive.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/include/rs-plugin/css/navigation.css">

  <?php wp_head(); ?>
<style>
.revo-slider-emphasis-text {
  font-size: 64px;
  font-weight: 700;
  letter-spacing: -1px;
  font-family: 'ASL-2', sans-serif;
  font-style: italic;
  padding: 15px 20px;
  border-top: 2px solid #FFF;
  border-bottom: 2px solid #FFF;
}

.revo-slider-desc-text {
  font-size: 20px;
  font-family: 'ASL-2', sans-serif;
  width: 650px;
  text-align: center;
  line-height: 1.5;
}

.revo-slider-caps-text {
  font-size: 16px;
  font-weight: 400;
  letter-spacing: 3px;
  font-family: 'ASL-2', sans-serif;
  font-style: italic;
}
.tp-video-play-button { display: none !important; }

.tp-caption { white-space: nowrap; }
</style>
</head>
<body class="stretched" data-loader="14" data-loader-color="#26325e" data-speed-in="1500" data-speed-out="800">

  <div class="body-overlay"></div>

  <!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

	<!-- Header
		============================================= -->
    <?php
    $header = '';
    if(is_front_page()){
      $header = 'transparent-header ';
    }
    ?>
		<header id="header" class="<?php echo $header; ?>full-header">

      <?php create_cm_menu("main-menu"); ?>

    </header><!-- #header end -->
