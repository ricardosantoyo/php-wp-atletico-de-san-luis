			<!-- renta de espacios -->
			<div class="section parallax dark notopmargin nobottommargin noborder" style="margin-top:0; padding: 30px 0;
			background-image: url('<?php echo bloginfo('template_directory'); ?>/images/recursosCasaMaestra/banner_rentadeespacios.jpg');" data-stellar-background-ratio="0.4">
					<div class="container center clearfix">

						<div class="emphasis-title">
							<h3 style="font-size: 40px !important;">Renta de Espacios</h3>
							<p class="lead topmargin-sm">Contamos con espacios de primera para todas las necesidades, <br>por hora o fijo.</p>

							<a href="renta-de-espacios" target="_self" class="button button-border button-white button-light button-large button-rounded tright nomargin">¡Conócelos aquí!</a>
						</div>

					</div>
				</div>
				<!-- fin renta de instalaciones -->
