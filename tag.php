<?php get_header(); ?>

<section id="page-title">

  <div class="container clearfix">
    <h1><center><strong>Etiqueta: <?php single_tag_title(); ?></strong></center></h1>
  </div>

</section><!-- #page-title end -->


<!-- Content
============================================= -->
<section id="content">

  <div class="content-wrap">

    <div class="container clearfix">

      <div id="posts" class="small-thumbs">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <?php get_template_part( 'tag', 'content' ); ?>

        <?php endwhile; endif; ?>

      </div>

    </div>

  </div>

</section>

<?php get_template_part( 'nav', 'below' ); ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
