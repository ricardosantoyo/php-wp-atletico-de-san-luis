<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<section id="page-title" class="page-title-parallax color" style="background-image: url('<?php bloginfo('template_directory'); ?>/images/secciones/responsabilidad/FONDO_POSTOSINOSENACCION.jpg'); padding: 120px 0;" data-stellar-background-ratio="0.3">
  <style>
  #page-title h1 {
    color: rgb(38,50,94) !important;
    font-family: 'ASL-3';
    font-size: 60px !important;
  }
  </style>
  <div class="container clearfix">
    <center>
      <img src="http://atleticodesanluis.mx/wp-content/uploads/2017/12/cropped-logo_colorgrande.png" class="img-responsive" width="20%" />
      <br />
      <h1><?php echo get_the_title(); ?></h1>
    </center>
  </div>

</section><!-- #page-title end -->

<section id="content">

  <div class="content-wrap">

    <?php
    $full_width = get_post_meta( get_the_ID(), 'full_width', true );
    $full_width = !empty( $full_width ) ? ( ( $full_width === "true" ) ? true : false ) : false;

    if($full_width){
    ?>
    <div class="container-fluid clearfix">
    <?php } else { ?>
    <div class="container clearfix">
    <?php } ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <section class="entry-content">
        <?php the_content(); ?>
        <div class="entry-links"><?php wp_link_pages(); ?></div>
      </section>
      </article>
    </div>
  </div>
</section>
<?php edit_post_link(); ?>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
