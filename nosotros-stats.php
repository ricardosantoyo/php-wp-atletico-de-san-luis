<?php
function facebook_shares($url, $token){
$url = $url . "&access_token=" . $token;
  //  Initiate curl
  $ch = curl_init();
  // Disable SSL verification
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  // Will return the response, if false it print the response
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  // Set the url
  curl_setopt($ch, CURLOPT_URL,$url);
  // Execute
  $result = curl_exec($ch);
$fb = json_decode($result, true);
  // Closing
  curl_close($ch);
  return $fb["fan_count"];
}

$fbtoken = "EAAHHVQUDyKQBAEG1IOn0BhP73xQLuNGbNpVuZCvmv8YlaZBh7sV5HleyabsvyQbMLoNajsZAnriiZBJJchVakYDbelyDQIXTbpWg4LmH51siUjIxhvbdHDbQ8DQ6340u0lxTZCxat7Q7uoo4NZBcOZCZA2rBWq9TzbbltNejyICdSgZDZD";

$fb = facebook_shares("https://graph.facebook.com/v2.9/casamaestra?fields=fan_count", $fbtoken);

$LIKES = $fb;

$now = time();
$your_date = strtotime("2010-03-01");
$datediff = $now - $your_date;

$DAYS = floor($datediff / (60 * 60 * 24));

?>
<div class="col_one_fourth center" data-animate="bounceIn">
  <i class="i-plain i-xlarge divcenter nobottommargin icon-like"></i>
  <div class="counter counter-large" style="color: #3498db;"><span data-from="500" data-to="<?php echo $LIKES; ?>" data-refresh-interval="50" data-speed="2000"></span></div>
  <h5>Seguidores en Facebook</h5>
</div>

<div class="col_one_fourth center" data-animate="bounceIn" data-delay="200">
  <i class="i-plain i-xlarge divcenter nobottommargin icon-calendar2"></i>
  <div class="counter counter-large" style="color: #e74c3c;"><span data-from="200" data-to="<?php echo $DAYS; ?>" data-refresh-interval="50" data-speed="2500"></span></div>
  <h5>Días de nacido</h5>
</div>

<div class="col_one_fourth center" data-animate="bounceIn" data-delay="400">
  <i class="i-plain i-xlarge divcenter nobottommargin icon-line2-user-female"></i>
  <div class="counter counter-large" style="color: #16a085;"><span data-from="500000" data-to="1250325" data-refresh-interval="50" data-speed="3500"></span></div>
  <h5>Mamás felices</h5>
</div>

<div class="col_one_fourth center col_last" data-animate="bounceIn" data-delay="600">
  <i class="i-plain i-xlarge divcenter nobottommargin ninollorando"></i>
  <div class="counter counter-large" style="color: #9b59b6;"><span data-from="-500000" data-to="-1250325" data-refresh-interval="30" data-speed="2700"></span></div>
  <h5>Niños berrinchudos</h5>
</div>
